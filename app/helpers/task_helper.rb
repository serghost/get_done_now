# Helper methods defined here can be accessed in any controller or view in the application

module GetDoneNow
  class App
    module TaskHelper
        # def simple_helper_method
      # ...
      # end
    end

    helpers TaskHelper
  end
end
