class AddProjectToTask < ActiveRecord::Migration
  def self.up
    change_table :tasks do |t|
      t.integer :project_id
    end
  end

  def self.down
    change_table :tasks do |t|
      t.remove :project_id
    end
  end
end
