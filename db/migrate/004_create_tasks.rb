class CreateTasks < ActiveRecord::Migration
  def self.up
    create_table :tasks do |t|
      t.string :title
      t.date :start
      t.date :soft_deadline
      t.date :hard_deadline
      t.boolean :done
      t.integer :priority
      t.string :tag
      t.text :body
      t.timestamps
    end
  end

  def self.down
    drop_table :tasks
  end
end
